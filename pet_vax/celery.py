import os
from celery import Celery
from celery.schedules import crontab
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pet_vax.settings.local')

app = Celery('pet_vax', broker='amqp://localhost')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()

app.conf.beat_schedule = {
    'get_reminders': {
        'task': 'reminders',
        'schedule': crontab(),
    }
}

app.conf.timezone = 'America/Bogota'
