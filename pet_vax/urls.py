from django.contrib import admin
from django.urls import (path,
                         re_path,
                         include,
                         )
from django.conf import settings
from django.conf.urls.static import static
from applications.users import views
from rest_framework_simplejwt.views import (
    TokenRefreshView,
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path(
        'api/token/',
        views.MyTokenObtainPairView.as_view(),
        name='token_obtain_pair'
    ),
    path(
        'api/token/refresh/',
        TokenRefreshView.as_view(),
        name='token_refresh'
    ),
    path(
        '',
        include('applications.location.urls')
    ),
    path(
        '',
        include('applications.users.urls')
    ),
    path(
        '',
        include('applications.pet.urls')
    ),
    path(
        '',
        include('applications.vaccine.urls')
    ),
    path(
        '',
        include('applications.vet.urls')
    ),
    path(
        '',
        include('applications.reminders.urls')
    ),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
