from django.urls import path

from . import views

urlpatterns = [
    path(
        'register_vet/',
        views.VetSignUpCreateView.as_view(),
        name='register_vet',
    ),
]
