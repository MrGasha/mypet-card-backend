from django.shortcuts import render
from rest_framework.generics import (
    CreateAPIView
)
from rest_framework.permissions import IsAuthenticated
from .models import Vet
from .serializers import VetSerializer

# Create your views here.


class VetSignUpCreateView(CreateAPIView):
    serializer_class = VetSerializer

    permission_classes = [IsAuthenticated]
    queryset = Vet.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
