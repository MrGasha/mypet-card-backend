from django.db import models

from applications.users.models import User
from applications.pet.models import Pet
from .managers import VetManager
# Create your models here.


class Vet(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='user')

    clinic_name = models.CharField(max_length=50, blank=True, null=True)

    graduation_date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True,
    )

    pets_allowed = models.ManyToManyField(
        Pet,
        blank=True,
    )

    objects = VetManager()

    class Meta:
        verbose_name = 'Vet'
        verbose_name_plural = 'Vets'

    def __str__(self):
        return str(self.user)
