from django.db import models


class PetManager(models.Manager):
    def get_pet_by_user(self, user_id):
        result = self.filter(owner=user_id)
        return result
