from django.shortcuts import render
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
)
from rest_framework.permissions import IsAuthenticated
from .models import Pet
from .serializers import (
    PetSerializer,
)

# Create your views here.


class PetlistView(ListAPIView):
    serializer_class = PetSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user_id = self.request.user.id
        return Pet.objects.get_pet_by_user(user_id)


class PetCreateView(CreateAPIView):
    serializer_class = PetSerializer
    permission_classes = [IsAuthenticated]
    queryset = Pet.objects.all()
    """
    Perform_create gets called while create() is executing, 
    for saving owner one must exclude owner from serializer,
    because validation is called before perform_create(),
    validation return error because it expect an owner,
    other option is override data at create()
    so it directly recieves current user and all fields 
    are preserved on petserializer.
    as for feb 5 of 2022 this seems not a problem
    """

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
