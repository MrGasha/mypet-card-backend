from django.urls import path

from . import views

urlpatterns = [
    path(
        'get_pets_by_user/',
        views.PetlistView.as_view(),
        name='get_pets_by_user',
    ),
    path(
        'register_pet/',
        views.PetCreateView.as_view(),
        name='register_pet',
    ),
]
