from django.db import models

from applications.users.models import User

from .managers import PetManager
# Create your models here.


class Pet(models.Model):

    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )

    male = 1
    female = 2
    other = 3

    gender_choices = (
        (male, 'male'),
        (female, 'female'),
        (other, 'other')
    )

    cat = 1
    dog = 2
    another = 3  # might be a cow

    species_choices = (
        (dog, 'dog'),
        (cat, 'cat'),
        (another, 'another')
    )

    pet_name = models.CharField(max_length=50)

    pet_birthdate = models.DateField(
        auto_now=False,
        auto_now_add=False
    )

    species = models.PositiveIntegerField(
        'species',
        choices=species_choices,
        blank=True,
        null=True
    )

    gender = models.PositiveIntegerField(
        'gender',
        choices=gender_choices,
        blank=True,
        null=True,
    )

    pet_description = models.CharField(max_length=310)

    pet_pic = models.ImageField(
        upload_to='pet_pic/',
        blank=True,
        null=True
    )

    objects = PetManager()

    class Meta:
        verbose_name = 'Pet'
        verbose_name_plural = 'Pets'

    def __str__(self):
        return self.pet_name
