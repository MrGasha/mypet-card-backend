# Generated by Django 4.1 on 2022-08-05 11:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pet_name', models.CharField(max_length=50)),
                ('pet_birthdate', models.DateField()),
                ('species', models.PositiveIntegerField(blank=True, choices=[(1, 'dog'), (2, 'cat'), (3, 'another')], null=True, verbose_name='species')),
                ('gender', models.PositiveIntegerField(blank=True, choices=[(1, 'male'), (2, 'female'), (3, 'other')], null=True, verbose_name='gender')),
                ('pet_description', models.CharField(max_length=310)),
                ('pet_pic', models.ImageField(blank=True, null=True, upload_to='pet_pic/')),
            ],
            options={
                'verbose_name': 'Pet',
                'verbose_name_plural': 'Pets',
            },
        ),
    ]
