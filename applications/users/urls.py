from django.urls import path, re_path

from . import views


urlpatterns = [
    path(
        'signup/',
        views.SignUpCreateView.as_view(),
        name='signup'
    ),
    path(
        'get_user/',
        views.CurrentUserRetrieve.as_view(),
        name='get_user'
    ),
]
