from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from .models import User


class SignUpSerializer(serializers.ModelSerializer):

    def validate(self, data):
        return data

    class Meta:
        model = User
        fields = (
            'email',
            'password',
            'name',
            'last_name',
            'role',
            'gender',
            'birthdate',
            'profile_picture',
            'location',
        )

    def create(self, validated_data):
        auth_user = User.objects.create_user(**validated_data)
        return auth_user


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        data['user_id'] = self.user.id

        return data


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'name',
            'last_name',
            'profile_picture',
        )
