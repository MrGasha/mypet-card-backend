from django.shortcuts import (
    render,
    get_object_or_404,
)
from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
)
from .models import User
from .serializers import (
    MyTokenObtainPairSerializer,
    SignUpSerializer,
    UserSerializer,
)
from .managers import CustomUserManager
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.permissions import IsAuthenticated


class SignUpCreateView(CreateAPIView):
    serializer_class = SignUpSerializer


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class CurrentUserRetrieve(RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer

    def get_queryset(self):
        user_id = self.request.user.id
        return User.objects.get_user(user_id)

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset)
        return obj
