from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from applications.location.models import City

from .managers import CustomUserManager
# Create your models here.


class User(AbstractBaseUser, PermissionsMixin):

    Owner = 0
    Vet = 1

    Male = 1
    Female = 2
    Other = 3

    gender_choices = (
        (Male, 'Male'),
        (Female, 'Female'),
        (Other, 'Other'),
    )

    rol_choices = (
        (Owner, 'Owner'),
        (Vet, 'Vet'),
    )

    email = models.EmailField(
        unique=True,
    )

    name = models.CharField(max_length=50)

    last_name = models.CharField(max_length=50)

    role = models.PositiveIntegerField(
        'role',
        choices=rol_choices,
        blank=True,
        null=True
    )

    gender = models.PositiveSmallIntegerField(
        'gender',
        choices=gender_choices,
        blank=True,
        null=True
    )

    birthdate = models.DateField(
        'birthdate',
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True
    )

    profile_picture = models.ImageField(
        upload_to='profile_pic/',
        blank=True,
        null=True
    )

    joined_date = models.DateField(
        auto_now_add=True
    )

    location = models.ForeignKey(
        City,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    is_active = models.BooleanField(
        default=True
    )

    is_staff = models.BooleanField(
        default=False
    )

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return self.email
