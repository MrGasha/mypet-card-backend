# Generated by Django 4.1 on 2022-09-03 22:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_user_gender'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='gender',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(0, 'Male'), (1, 'Female'), (2, 'Other')], null=True, verbose_name='gender'),
        ),
        migrations.AlterField(
            model_name='user',
            name='role',
            field=models.PositiveIntegerField(blank=True, choices=[(0, 'Owner'), (1, 'Vet')], null=True, verbose_name='role'),
        ),
    ]
