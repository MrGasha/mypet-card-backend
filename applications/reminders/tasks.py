from celery import shared_task
from applications.vaccine.models import Vaccine
from django.utils import timezone
from .models import Reminder
import datetime

DELTA_24H = {
    'delta': datetime.timedelta(days=1),
    'type': 1,
}
DELTA_48H = {
    'delta': datetime.timedelta(days=2),
    'type': 2,
}
DELTA_1WEEK = {
    'delta': datetime.timedelta(days=7),
    'type': 3,
}
DELTA_1MONTH = {
    'delta': datetime.timedelta(days=30),
    'type': 4,
}
DELTA_3MONTH = {
    'delta': datetime.timedelta(days=90),
    'type': 5,
}


@shared_task(name='reminders')
def get_reminders():
    my_result = Vaccine.objects.get_vax_w_bdate()
    today = datetime.date.today()
    for vaccine in my_result:
        check_timeframe(vaccine, today)


def check_timeframe(vaccine, today):
    delta = vaccine.booster_date - today
    print(delta)
    # i believe this can be reduced to not repeat
    if delta <= DELTA_24H['delta']:
        check_prev_reminders(vaccine, DELTA_24H)

    elif delta <= DELTA_48H['delta']:
        check_prev_reminders(vaccine, DELTA_48H)

    elif delta <= DELTA_1WEEK['delta']:
        check_prev_reminders(vaccine, DELTA_1WEEK)

    elif delta <= DELTA_1MONTH['delta']:
        check_prev_reminders(vaccine, DELTA_1MONTH)

    elif delta <= DELTA_3MONTH['delta']:
        check_prev_reminders(vaccine, DELTA_3MONTH)


def check_prev_reminders(vaccine, time_frame):
    result = Reminder.objects.get_prev_reminder(vaccine.id)
    has_prev_reminder = False if not result else True
    print(vaccine.vaccine_name)
    if (has_prev_reminder and result.reminder_type == time_frame['type']):
        # If has reminder and reminder type is the one we have stored
        # Do nothing
        return
    elif (has_prev_reminder and result.reminder_type != time_frame['type']):
        # If has reminder and reminder type is NOT the one we have stored
        delete_prev_reminders(result)
        create_reminder(vaccine, time_frame['type'])
    else:
        # If has not reminder
        create_reminder(vaccine, time_frame['type'])


def create_reminder(vaccine, reminder_type):
    Reminder.objects.create_reminder(vaccine, reminder_type)
    pass


def send_email():
    pass


def delete_prev_reminders(reminder):
    Reminder.objects.delete_reminder(reminder)
    pass
