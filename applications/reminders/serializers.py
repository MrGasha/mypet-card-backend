from rest_framework import serializers
from .models import (
    Reminder,
)
from applications.vaccine.serializers import VaccineReminderSerializer


class ReminderSerializer(serializers.ModelSerializer):
    vaccine = VaccineReminderSerializer()

    class Meta:
        model = Reminder
        fields = ('__all__')
