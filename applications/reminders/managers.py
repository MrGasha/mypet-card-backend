from django.db import models


class ReminderManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter()

    def get_prev_reminder(self, vaccine_id):
        result = self.filter(vaccine=vaccine_id)
        return result.first()

    def create_reminder(self, vaccine, reminder_type):
        new_reminder = self.create(
            vaccine=vaccine,
            reminder_type=reminder_type
        )
        return new_reminder

    def delete_reminder(self, reminder):
        reminder.delete()
        return

    def get_reminders_by_user(self, user_id):
        result = self.filter(vaccine__pet__owner__id=user_id)
        return result
