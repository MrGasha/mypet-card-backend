from django.db import models
from applications.vaccine.models import Vaccine
from .managers import (
    ReminderManager
)
# Create your models here.


class Reminder(models.Model):
    vaccine = models.ForeignKey(Vaccine, on_delete=models.CASCADE,)
    reminder_24h = 1
    reminder_48h = 2
    reminder_1week = 3
    reminder_1month = 4
    reminder_3month = 5

    reminder_choices = (
        (reminder_24h, '24h reminder'),
        (reminder_48h, '48h reminder'),
        (reminder_1week, '1 week reminder'),
        (reminder_1month, '1 month reminder'),
        (reminder_3month, '3 month reminder'),
    )

    reminder_type = models.PositiveSmallIntegerField(
        'reminder_type',
        choices=reminder_choices,
    )

    objects = ReminderManager()

    class Meta:
        verbose_name = 'Reminder'
        verbose_name_plural = 'Reminders'

    def __str__(self):
        return str(self.reminder_type)
