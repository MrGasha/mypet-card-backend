from django.urls import path

from . import views

urlpatterns = [
    path(
        'get_reminders_by_user/',
        views.ReminderlistView.as_view(),
        name='get_reminders_by_user',
    ),

]
