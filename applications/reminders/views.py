from django.shortcuts import render
from rest_framework.generics import (
    ListAPIView,
)
from rest_framework.permissions import IsAuthenticated
from .models import Reminder
from .serializers import (
    ReminderSerializer,
)
# Create your views here.


class ReminderlistView(ListAPIView):
    serializer_class = ReminderSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user_id = self.request.user.id
        return Reminder.objects.get_reminders_by_user(user_id)
