from django.urls import path, re_path

from . import views

urlpatterns = [
    path(
        'get_countries/',
        views.CountryListView.as_view(),
        name='get_countries',
    ),
    path(
        'get_states_by_country/<pk>/',
        views.StateListView.as_view(),
        name='get_state_by_country',
    ),
    path(
        'get_cities_by_state/<pk>/',
        views.CityListView.as_view(),
        name='get_cities_by_state',
    ),
]
