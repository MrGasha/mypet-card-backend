from django.shortcuts import render
from rest_framework.generics import (ListAPIView)
from .models import (
    Country,
    State,
    City,
)
from .serializers import(
    CountrySerializer,
    StateSerializer,
    CitySerializer,
)
# Create your views here.


class CountryListView(ListAPIView):
    serializer_class = CountrySerializer

    def get_queryset(self):
        return Country.objects.get_countries()


class StateListView(ListAPIView):
    serializer_class = StateSerializer

    def get_queryset(self):
        country_id = self.kwargs['pk']
        return State.objects.get_states_by_country(country_id)


class CityListView(ListAPIView):
    serializer_class = CitySerializer

    def get_queryset(self):
        state_id = self.kwargs['pk']
        return City.objects.get_cities_by_state(state_id)
