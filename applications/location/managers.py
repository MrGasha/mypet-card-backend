from django.db import models

# I could use if self.model.__name__== 'Main' to change this into one manager for those models
# For now i will use this repeating solution


class CityManager(models.Manager):
    def get_cities_by_state(self, state_id):
        result = self.filter(state=state_id)
        return result

    def get_queryset(self):
        return super().get_queryset().filter()


class CountryManager(models.Manager):

    def get_countries(self):
        result = self.all()
        return result

    def get_queryset(self):
        return super().get_queryset().filter()


class StateManager(models.Manager):

    def get_states_by_country(self, country_id):
        result = self.filter(country=country_id)
        return result

    def get_queryset(self):
        return super().get_queryset().filter()
