from django.db import models
from .managers import (
    CountryManager,
    StateManager,
    CityManager,
)
# Create your models here.


class City(models.Model):

    city_name = models.CharField(max_length=60)
    state = models.ForeignKey(
        "State",
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'

    objects = CityManager()

    def __str__(self):
        return self.city_name


class State(models.Model):

    state_name = models.CharField(max_length=60)
    country = models.ForeignKey(
        "Country",
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = 'State'
        verbose_name_plural = 'States'

    objects = StateManager()

    def __str__(self):
        return self.state_name


class Country(models.Model):

    country_name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Countries'

    objects = CountryManager()

    def __str__(self):
        return self.country_name
