from django.db import models
from django.utils import timezone


class VaccineManager(models.Manager):
    def get_vaccines_by_pet(self, pet_id):
        result = self.filter(pet=pet_id).order_by('application_date')
        return result
    """
    gets vaccines with one time false and boosterdates after today
    """

    def get_vax_w_bdate(self):
        today = timezone.now()
        result = self.filter(one_time=False, booster_date__gte=today)
        return result
