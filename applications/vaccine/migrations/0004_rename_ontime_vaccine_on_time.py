# Generated by Django 4.1 on 2022-09-20 21:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vaccine', '0003_remove_vaccine_booster_date_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vaccine',
            old_name='onTime',
            new_name='on_time',
        ),
    ]
