# Generated by Django 4.1 on 2022-09-23 16:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vaccine', '0004_rename_ontime_vaccine_on_time'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vaccine',
            old_name='on_time',
            new_name='one_time',
        ),
        migrations.RemoveField(
            model_name='vaccine',
            name='dosage',
        ),
    ]
