from django.shortcuts import render
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    DestroyAPIView,
)
from rest_framework.permissions import IsAuthenticated
from .models import Vaccine
from .serializers import (
    VaccineSerializer,
)

# Create your views here.


class VaccineCreateView(CreateAPIView):
    serializer_class = VaccineSerializer
    permission_classes = [IsAuthenticated]
    queryset = Vaccine.objects.all()


class VaccineListView(ListAPIView):
    serializer_class = VaccineSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        pet_id = self.kwargs['pk']
        return Vaccine.objects.get_vaccines_by_pet(pet_id=pet_id)


class VaccineDestroyAPIView(DestroyAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = VaccineSerializer
    queryset = Vaccine.objects.all()


