from rest_framework import serializers
from .models import (
    Vaccine,
)
from applications.pet.serializers import PetReminderSerializer


class VaccineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vaccine
        fields = ('__all__')


class VaccineReminderSerializer(serializers.ModelSerializer):
    pet = PetReminderSerializer()

    class Meta:
        model = Vaccine
        fields = (
            'id',
            'vaccine_name',
            'application_date',
            'booster_date',
            'pet'
        )
