from django.urls import path

from . import views

urlpatterns = [
    path(
        'add_vax/',
        views.VaccineCreateView.as_view(),
        name='add_vax',
    ),
    path(
        'get_vax_by_pet/<pk>/',
        views.VaccineListView.as_view(),
        name='get_vax_by_pet',
    ),
    path(
        'delete_vax/<pk>/',
        views.VaccineDestroyAPIView.as_view(),
        name='delete_vax',
    ),

]
