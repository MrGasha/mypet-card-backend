from django.db import models

from applications.vet.models import Vet
from applications.pet.models import Pet
from .managers import VaccineManager
# Create your models here.


class Vaccine(models.Model):
    pet = models.ForeignKey(Pet, on_delete=models.CASCADE)
    vaccine_name = models.CharField(max_length=60)
    application_date = models.DateField(
        auto_now=False,
        auto_now_add=False,
    )
    booster_date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        null=True,
    )

    one_time = models.BooleanField(default=False)
    tag_pic = models.ImageField(
        upload_to='vax_tag/',
        blank=True,
        null=True
    )
    vax_description = models.CharField(max_length=300)
    vet = models.ForeignKey(
        Vet,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    objects = VaccineManager()

    class Meta:
        verbose_name = 'Vaccine'
        verbose_name_plural = 'Vaccines'

    def __str__(self):
        return self.vaccine_name
